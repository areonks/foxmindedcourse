$(function () {
$("#complForm").validate({
        rules: {
            login: {
                required: true ,
                minlength: 3 ,
                maxlength: 20 ,
            } ,
            mail: {
                email: true ,
                required: true ,
                minlength: 3 ,
                maxlength: 20 ,
            } ,
            coment: {
                required: true ,
                minlength: 3 ,
                maxlength: 20 ,
            }
        },
        submitHandler: function (){
            let log = $('#date').val() ,
                name = $('#airport').val() ,
                compTxt = $('#desc').val();
            $.post('index.php' , {log , name , compTxt})
                .done(function (data) {
                    alert(data);
                })
        }
    });
})