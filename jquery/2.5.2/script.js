$(function () {
    $(".ui-draggable").draggable({
        revert: "invalid" ,
        containment: "document"
    });
    $("#droppable").droppable({
        classes: {
            "ui-droppable-hover": "ui-state-hover"
        } ,
        drop: function (event , ui) {
            ui.draggable.remove();

            $(".epuff").animate({
                marginTop: '-746px'
            } , 340 , function () {
                $(this).css('margin-top' , '42px')
                ;

            });

        }
    })
    $.fx.interval = 40;
})

