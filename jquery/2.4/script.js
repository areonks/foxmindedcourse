$(function () {
    let availAirports = [
        "ActionScript" ,
        "AppleScript" ,
        "Asp" ,
        "BASIC" ,
        "C" ,
        "C++" ,
        "Clojure" ,
        "COBOL" ,
        "ColdFusion" ,
        "Erlang" ,
        "Fortran" ,
        "Groovy" ,
        "Haskell" ,
        "Java" ,
        "JavaScript" ,
        "Lisp" ,
        "Perl" ,
        "PHP" ,
        "Python" ,
        "Ruby" ,
        "Scala" ,
        "Scheme"
    ]
    $(".ui-widget button")
    // .eq
    $("#meal").selectmenu()
    $(".bags").checkboxradio({
        icon: false
    });
    $("#seatsBlock").controlgroup();

    $("#frsBlock").controlgroup();
    $(".seats").checkboxradio({
        icon: false
    });

    $("#airport").autocomplete({
        source: availAirports
    });
    $("#date").datepicker();
});